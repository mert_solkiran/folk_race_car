function [area, gradient] = rebellion(qstar, obstacles, count, max, maxgra, coeff, coeff2, size)

area = -1 * ones(size, size);
gradient = -1 * ones(size, size, 2);
for z = 1:size
    for y = 1:size
        area(z,y) = 0;
        for x=1:count
            current = obstacles{x};
            
            distance = sqrt((z - current(1))^2 + (y - current(2))^2) - current(3);
            if distance <= 0
                area(z,y) = max;
                gradient(z,y,:) = [0 0];
                break;
            elseif distance < qstar
                area(z,y) = area(z,y) + 0.5 * coeff * (1/distance - 1/qstar).^2;
                if area(z,y) > max
                    coeff = max;
                end
                gradient(z,y,1) = gradient(z,y,1) + coeff2 * (1/qstar - 1/distance) / distance^2 * (z - current(1));
                gradient(z,y,2) = gradient(z,y,2) + coeff2 * (1/qstar - 1/distance) / distance^2 * (y - current(2));
            end
        end 
        
        sum = sqrt(gradient(z,y,1)^2 + gradient(z,y,2)^2);
        if sum  > maxgra
            gradient(z,y,1) = (gradient(z,y,1) / sum) * maxgra;
            gradient(z,y,2) = (gradient(z,y,2) / sum) * maxgra;
        end
        
    end
end