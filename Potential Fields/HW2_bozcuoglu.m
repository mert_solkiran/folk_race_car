function varargout = HW2_bozcuoglu(varargin)
% HW2_BOZCUOGLU M-file for HW2_bozcuoglu.fig
%      HW2_BOZCUOGLU, by itself, creates a new HW2_BOZCUOGLU or raises the existing
%      singleton*.
%
%      H = HW2_BOZCUOGLU returns the handle to a new HW2_BOZCUOGLU or the handle to
%      the existing singleton*.
%
%      HW2_BOZCUOGLU('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HW2_BOZCUOGLU.M with the given input arguments.
%
%      HW2_BOZCUOGLU('Property','Value',...) creates a new HW2_BOZCUOGLU or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before HW2_bozcuoglu_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to HW2_bozcuoglu_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help HW2_bozcuoglu

% Last Modified by GUIDE v2.5 02-Mar-2010 23:59:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @HW2_bozcuoglu_OpeningFcn, ...
                   'gui_OutputFcn',  @HW2_bozcuoglu_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before HW2_bozcuoglu is made visible.
function HW2_bozcuoglu_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to HW2_bozcuoglu (see VARARGIN)
axis([0 100 0 100]);
size = 100;
handles.size = size;
big_circle = [size/2 size/2 size / 2 - 10];
[X Y] = circle([size/2,size/2],size/2,100);
plot(X, Y,'b');

handles.counter = 0;
% Choose default command line output for HW2_bozcuoglu
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes HW2_bozcuoglu wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = HW2_bozcuoglu_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hold on;
radius = 3 + rand(1)* 7;
xx = 30 + rand(1)* 40;
yy = 30 + rand(1)* 40;

[X Y] = circle([xx, yy],radius,100);
plot(X, Y,'b');
handles.counter = handles.counter + 1;
handles.obstacles{handles.counter} = [xx yy radius];

guidata(hObject,handles)



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
frame = getframe;
[X, Map] = frame2im(frame);
handles.img = X(:,:,1);
point = ginput(1);
handles.x(1)=point(1,1);
handles.y(1)=point(1,2);
rectangle('Position',[point-[.01, .01], .02, .02],'Curvature',[1,1],'FaceColor','r')
point = ginput(1);
handles.x(2)=point(1,1);
handles.y(2)=point(1,2);
rectangle('Position',[point-[.01, .01], .02, .02],'Curvature',[1,1],'FaceColor','r')

guidata(hObject,handles)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[a b t area2] = navigatial(handles.obstacles, handles.counter, handles.size, [handles.x(2) handles.y(2)], [handles.x(1) handles.y(1)]);
[x y t area rarea isField] = potential(handles.obstacles, handles.counter, handles.size, [handles.x(2) handles.y(2)], [handles.x(1) handles.y(1)]);
hold on;
plot(a,b, 'r');
hold on;
plot(x,y, 'g');


figure;
surf (area); figure(gcf)
axis square;
title('Attractive Potential Field');
figure;
surf (rarea); figure(gcf)
axis square;
title('Repulsive Potential Field');
figure;
contourf (area); figure(gcf)
axis square;
title('2-D Attractive Potential Field');
figure;
contourf (rarea); figure(gcf)
axis square;
title('2-D Repulsive Potential Field');
figure;
surf ((area + rarea)); figure(gcf)
axis square;
title('Total Potential Field');
figure;
contourf ((area + rarea)); figure(gcf)
axis square;
title('2-D Total Potential Field');
surf (area2); figure(gcf)
axis square;
title('Total Navigation Function Field');
figure;
contourf (area2); figure(gcf)
axis square;
title('2-D Total Navigation Function Field');

