function [ hst ] = im2Hist( img , format,bin_level )
n = bin_level;

if (strcmp(format,'rgb'))
    img = rgb2hsv( img );
    hst = hist( reshape(img, [], 3), 255 ); % instead of loop!
    hst = hst(:) / n;
elseif (strcmp(format,'hsv'))
    img = img;
    hst = hist( reshape(img, [], 3), 255 ); % instead of loop!
    hst = hst(:) / n;
elseif (strcmp(format,'gray') || strcmp(format,'grey'))
    hst = hist(img,n);
else
    hst = hist(img,n);
end




end

