function [ plane1, plane2, plane3, plane4, cam_org, lines, HRES, VRES ] = camera(view_depth)
format long



cam_org = [0;0;0]; % XYZ coordinates of camera origin. 
% X axis represents horizontal field of view
% Y axis represents Vertical field of view
% Z axis represents depth of camera view

HRES = 3280; % horizontal pixels
VRES = 2464; % vertical pixels

focal_length = 3.04; %% mm
HFOV = 62.2; % horizontal FOV degree
VFOV = 48.8; % vertical FOV degree

HIMGAREA = 3.68; % Horizontal image area mm
VIMGAREA = 2.76; % Vertical image area mm
HPIXSIZE = 0.0012; % Horizontal pixel size mm
VPIXSIZE = 0.0012; % Vertical pixel size mm

% error_H = (HIMGAREA - (HPIXSIZE*HRES));
% error_V = (VIMGAREA - (VPIXSIZE*VRES));
% 
% HFOV_TEST = atand((HIMGAREA / 2)/ focal_length) * 2;
% VFOV_TEST = atand((VIMGAREA / 2)/ focal_length) * 2;

p1 = cam_org;
p1(3) = p1(3) + view_depth;
p1(2) = p1(2) + (p1(3) * tand(VFOV/2));
p1(1) = p1(1) + (p1(3) * tand(HFOV/2));

p2 = ((p1 - cam_org) .* [-1; 1; 1]) + cam_org;

p3 = ((p1 - cam_org) .* [1; -1; 1]) + cam_org;

p4 = ((p2 - cam_org) .* [1; -1; 1]) + cam_org; 

plane1 = [cam_org, p1, p2, cam_org ];
plane2 = [cam_org, p3, p4, cam_org ];
plane3 = [cam_org, p1, p3, cam_org ];
plane4 = [cam_org, p2, p4, cam_org ];

lines = [cam_org, p1, p2, p3, p4];



% fill3(plane1(1,:),plane1(2,:),plane1(3,:),'r',plane2(1,:),plane2(2,:),plane2(3,:),'r',plane3(1,:),plane3(2,:),plane3(3,:),'b',plane4(1,:),plane4(2,:),plane4(3,:),'b')
% grid on
% alpha(0.3)

% p1(1) = p1(1) + p1(3) / cosd(VFOV/2);

