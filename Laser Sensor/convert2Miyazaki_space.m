function [ output_args ] = convert2Miyazaki_space( input_args, alpha )
% Reference;
% http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche40.html



transforma_matrix = [1,-0.5, -0.5; 0, ((3)^0.5)/2, -((3)^0.5)/2; 1/3, 1/3, 1/3];
inv_transform_matrix = [2/3, 0, 1; -1/3, 1/(3^0.5), 1; -1/3, -1/(3^0.5), 1];
% m_matrix = transformation_matrix * [r_channel ; g_channel; b_channel];
m_matrix = reshape((transforma_matrix * reshape(input_args, [], 3).').', size(input_args));

m1_channel = m_matrix(:,:,1);
m2_channel = m_matrix(:,:,2);
m3_channel = m_matrix(:,:,3);

S = (((m1_channel.^2) + (m2_channel.^2)).^0.5);
S(S<0.0000001) = 0.0000001;
const = 1./S;

m1_channel = const .* m1_channel .* 255;
% m1_channel(m1_channel<0) = 0;
% m1_channel(m1_channel>255) = 255;
m2_channel = const .* m2_channel .* 255 .* ((3^0.5)/2);
% m2_channel(m2_channel<0) = 0;
% m2_channel(m2_channel>255) = 255;
m3_channel = m3_channel.*0 + 127;
m_matrix = cat(3,m1_channel,m2_channel,m3_channel);
% m3_channel = alpha.*((m1_channel.^2 + m2_channel.^2).^0.5);
% m_matrix(:,:,3) = m3_channel;
m_matrix = reshape((inv_transform_matrix * reshape(m_matrix, [], 3).').', size(m_matrix));


output_args = m_matrix;
end

