clear
clc

close all

origin_of_vehicle = [-400;1300];
rotation_angle = 3.1415; 
initial_velocity = 0; %mm/s
velocity_angle = 0;  %%rad
steering_angle = 0; % initial steering angle = 0 radian
desired_steering_angle = 0;
desired_acceleration = 100; %percent of max acceleration
sampling_interval = 0.05; %second
end_time = 2; %second
yaw_rate = [0,0,0]; %%rad/s
wheel_velocity = [0,0,0];
velocity_angle_front = [0,0,0];
acc_x = [0,0,0];
acc_y = [0,0,0];
vel_x = [0,0,0];
vel_y = [0,0,0];
velocity_angle_rear = velocity_angle_front;
rotation_angle = [rotation_angle,rotation_angle, rotation_angle];
[Ellipse_track_1, Ellipse_track_2] = Track_model_v2();

figure('units','normalized','outerposition',[0 0 1 1])


error = [];
laser_points_weight = [];
velocity = initial_velocity;

for (cntr = 1:(end_time/sampling_interval))
  clf
  [sharp_sen_arr_pnts, corner_points,origin_of_laser , laser_matrix, sharp_distance_matrix, laser_intersection_matrix, sharp_intersection_matrix, laser_measurement_matrix, rotation_angle, yaw_rate, steering_angle, velocity, velocity_angle, wheel_velocity, velocity_angle_front, velocity_angle_rear, acc_x, acc_y, vel_x, vel_y] = Vehicle_model(origin_of_vehicle, Ellipse_track_1, Ellipse_track_2, rotation_angle, yaw_rate, steering_angle, velocity, velocity_angle, desired_steering_angle, desired_acceleration, sampling_interval, wheel_velocity, velocity_angle_front, velocity_angle_rear, acc_x, acc_y, vel_x, vel_y);
  y_vector = (vel_y(end)*sampling_interval)/2;
  x_vector = (vel_x(end)*sampling_interval)/2;
  origin_of_vehicle = origin_of_vehicle + [x_vector; y_vector];


hold on
plot(Ellipse_track_1(1,:), Ellipse_track_1(2,:), Ellipse_track_2(1,:), Ellipse_track_2(2,:));
second = cntr*sampling_interval;
disp(["Current Angle:",mat2str(steering_angle),"Desired Angle:" mat2str(desired_steering_angle), " Time:" , mat2str(second)])

points_array = [sharp_sen_arr_pnts(1:2,:),sharp_sen_arr_pnts(3:4,:), corner_points , laser_matrix(1:2,:), laser_matrix(3:4,:), laser_intersection_matrix, sharp_intersection_matrix];
x_lims_min = min(points_array(1,:));
x_lims_max = max(points_array(1,:));
y_lims_min = min(points_array(2,:));
y_lims_max = max(points_array(2,:));

xlim([x_lims_min-500 x_lims_max+500])
ylim([y_lims_min-500 y_lims_max+500])
%axis("equal");
pbaspect([1 1 1]);

vehicle_plot = plot(corner_points(1,:), corner_points(2,:));
sharp_linewidth = 3;
sharp_plot = plot(sharp_sen_arr_pnts([1,3],1), sharp_sen_arr_pnts([2,4],1),"linewidth", sharp_linewidth , sharp_sen_arr_pnts([1,3],2), sharp_sen_arr_pnts([2,4],2),"linewidth", sharp_linewidth , sharp_sen_arr_pnts([1,3],3), sharp_sen_arr_pnts([2,4],3),"linewidth", sharp_linewidth , sharp_sen_arr_pnts([1,3],4), sharp_sen_arr_pnts([2,4],4),"linewidth", sharp_linewidth );
legend (sharp_plot(:), {"Sharp 1", "Sharp 2", "Sharp 3", "Sharp 4"});

[rows, columns] = size(laser_matrix);

  for cntr_rays = 1:columns
    rearranged_laser_matrix_x = [laser_matrix(1,cntr_rays),laser_matrix(3,cntr_rays)];
    rearranged_laser_matrix_y = [laser_matrix(2,cntr_rays),laser_matrix(4,cntr_rays)];
    laser_rays_plot = plot(rearranged_laser_matrix_x,rearranged_laser_matrix_y);
  endfor

  if ~isempty(laser_intersection_matrix)
  laser_intersect_plot = scatter(laser_intersection_matrix(1,:),laser_intersection_matrix(2,:));
  endif

  if ~isempty(sharp_intersection_matrix)
  sharp_intersect_plot = scatter(sharp_intersection_matrix(1,:),sharp_intersection_matrix(2,:));
  endif

  left_mean = (sharp_distance_matrix(1) + sharp_distance_matrix(4))/2;
  right_mean = (sharp_distance_matrix(2) + sharp_distance_matrix(3))/2;
  error = [error,(left_mean - right_mean)]/10000;
  laser_measurements_wo_NaN = laser_measurement_matrix(:,all(~isnan(laser_measurement_matrix))); 
  laser_point_dist = 4096.^((laser_measurements_wo_NaN(1,:)/20000));
  laser_points_weight = [laser_points_weight, (-1*sum(laser_measurements_wo_NaN(2,:)./laser_point_dist))];
  Kp = 2;
  Kd = 1;
  Kp_laser = 0;
  Kd_laser = 0;
    if (cntr>2)
      derivative_term = error(end) - error(end-1);
      derivative_term_laser = laser_points_weight(end) - laser_points_weight(end-1);
    else
      derivative_term = 0;
      derivative_term_laser = 0;
    endif
%      desired_steering_angle =  (Kp*error(end)) + (derivative_term*Kd) + (Kp_laser*laser_points_weight(end)) + (Kd_laser*derivative_term_laser);
desired_steering_angle = -0.5;
%      rotation_angle =  rotation_angle +(Kp_laser*laser_points_weight(end)) + (Kd_laser*derivative_term_laser);

pause(0.1)
endfor