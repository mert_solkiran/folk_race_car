function [area, gradient, force, isField] = attraction(size, goal, diststar, coeff)
area = -1 * ones(size, size);
force = -1 * ones(size, size);
gradient = -1 * ones(size, size);
isField = zeros(size, size);

for x = 1:size
    for y = 1:size
        if (x-size/2)^2+(y-size/2)^2 <= (size/2)^2
            
            distance = sqrt((x - goal(1))^2 + (y - goal(2))^2);

            if distance < diststar
                area(x,y) = coeff * distance^2 / 2;
                gradient(x,y, 1) = (x - goal(1));
                gradient(x,y, 2) = (y - goal(2));
            else
                area(x,y) = diststar * diststar * distance - coeff * diststar^2 / 2;
                gradient(x,y, 1) = diststar * diststar * (x - goal(1)) / distance;
                gradient(x,y, 2) = diststar * diststar * (x - goal(2)) / distance;
            end
            isField(x,y) = 1;
        end
    end
end