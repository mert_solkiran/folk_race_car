function [x y time area2] = navigatial(obstacles, count, size, goal, start)
global obs;
global cnt;
global gl;
gl = goal;
obs = obstacles;
cnt = count;

refine = 4;
options = odeset('Events',@stopCondition2,'RelTol',1e-6,'AbsTol',1e-8,'Refine',refine);

[time,axises,te,ye,ie] = ode45(@navigatialg, [0 1e8], start, options);

x= axises(:,1);
y=  axises(:,2);
tic;
area2 = navigation(obstacles, count, 2.7, 100)
zet = toc;
zet = 0;

% function gradiant = navigatialg(t,y)
% global gl kapa;
% 
% z = y';
% distance2goal=norm(z-gl);
% distance2obstacle=ObstacleDistance(z);
% gradiant=(2*(z-gl)*(distance2goal^(2*kapa)+distance2obstacle)^(1/kapa)-...
%           distance2goal^2*(1/kapa)*(distance2goal^(2*kapa) + distance2obstacle)^(1/kapa -1)*(2*kapa*distance2goal^(2*kapa-2)*(z-gl)+ObstacleDistanceGradiant(z)))...
%           /(distance2obstacle^(2*kapa)+distance2obstacle)^(2/kapa);
% gradiant = -gradiant';
% 
% function [sum,isterminal,direction] = stopCondition2(t,y)
% global gl kapa;
% z = y';
% cond = 1e3;
% distance2goal=norm(z-gl);
% distance2obstacle=ObstacleDistance(z);
% gradiant=(2*(z-gl)*(distance2goal^(2*kapa)+distance2obstacle)^(1/kapa)-...
%           distance2goal^2*(1/kapa)*(distance2goal^(2*kapa) + distance2obstacle)^(1/kapa -1)*(2*kapa*distance2goal^(2*kapa-2)*(z-gl)+ObstacleDistanceGradiant(z)))...
%           /(distance2obstacle^(2*kapa)+distance2obstacle)^(2/kapa);
%  gradiant = -gradiant';     
%       sum = norm(gradiant);
%       if sum < cond
%           sum = 1;
%       else
%           sum = 0;
%       end
%       isterminal = 1;   % Stop the integration
%       direction = 0;   % any direction

   
 

function deriv2 = navigatialg(t,z)
global obs;
global cnt;
global gl;
%coeff = 2;
 coeff = 2.7;
point = z';
deriv = zeros(2,1);
beta = -1 * ones(cnt + 1);
deltabeta = -1 * ones(cnt + 1, 2);

beta(1) =  2500 - (50 - point(1))^2 - (50 - point(2))^2;
deltabeta(1, 1) = -2 * (point(1) - 50);
deltabeta(1, 2) = -2 * (point(2) - 50);
% for y=1:cnt
%     cur2 = obs{y};
%     deltabeta(1, 1) = deltabeta(1, 1) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%     deltabeta(1, 2) = deltabeta(1, 2) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%     
% end

for x=1:cnt
    current = obs{x};
    beta(x + 1) = - current(3)^2 + (current(1) - point(1))^2 + (current(2) - point(2))^2;
    deltabeta(x + 1, 1) = 2 * (point(1) - current(1));
    deltabeta(x + 1, 2) = 2 * (point(2) - current(2));
    

%     for y=0:cnt
%         if y == x
%             continue;
%         elseif y == 0
%             deltabeta(x + 1, 1) = deltabeta(x + 1, 1) * beta(1);
%             deltabeta(x + 1, 2) = deltabeta(x + 1, 2) * beta(1);
%         else
%             cur2 = obs{y};
%             deltabeta(x + 1, 1) = deltabeta(x + 1, 1) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%             deltabeta(x + 1, 2) = deltabeta(x + 1, 2) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%         end
%     end
end
for x=1:cnt+1 
    for y=1:cnt+1
        if x == y
            continue;
        else
            deltabeta(x, :) = deltabeta(x, :) * beta(y); 
        end
            
    end
    
end

mbeta = beta(1);
sdbeta(1) = deltabeta(1, 1);
sdbeta(2) = deltabeta(1, 2);
for x=1:cnt
    mbeta = mbeta * beta(x + 1);
    sdbeta(1) = sdbeta(1) + deltabeta(x + 1, 1);
    sdbeta(2) = sdbeta(2) + deltabeta(x + 1, 2);
end
Dbeta(1) = sdbeta(1);
Dbeta(2) = sdbeta(2);

Dspec = (1/coeff)*(sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2)^(2*coeff)+mbeta)^(1/coeff-1) * (2* coeff * ((point(1) - gl(1))^2 + (point(2) - gl(2))^2)^(coeff-1)* (point-gl) + Dbeta) ;

deriv =(2* (point-gl) * (sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2)^(2*coeff)+mbeta)^(1/coeff) - ((point(1) - gl(1))^2 + (point(2) - gl(2))^2) * Dspec) / (sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2)^(2*coeff)+mbeta)^(2/coeff);
deriv2 = deriv';
deriv2(1) = -deriv(1);
deriv2(2) = -deriv(2);
% A = (point(1) - gl(1))^2 + (point(2) - gl(2))^2;
% Dz(1) = (point(1) - gl(1)) / sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2);
% Dz(2) = (point(2) - gl(2)) / sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2);
% DA(1) = 2 * sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2) * Dz(1);
% DA(2) = 2 * sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2) * Dz(1);
% 
% B = ((gl(1) - point(1))^2 + (gl(2) - point(2))^2)^coeff + mbeta;
% DB(1) = 2 * coeff *  ((gl(1) - point(1))^2 + (gl(2) - point(2))^2)^(coeff-0.5) * Dz(1) + Dbeta(1);
% DB(2) = 2 * coeff *  ((gl(1) - point(1))^2 + (gl(2) - point(2))^2)^(coeff-0.5)* Dz(2) + Dbeta(2);
% 
% 
% deriv(1) = DA(1) * B^(-1/coeff) + A * (-1 / coeff) * B^((-1-coeff)/coeff)* DB(1);
% deriv(2) = DA(2) * B^(-1/coeff) + A * (-1 / coeff) * B^((-1-coeff)/coeff)* DB(2);
% deriv(1) = -1*deriv(1);
% deriv(2) = -1*deriv(2);










function [sum,isterminal,direction] = stopCondition2(t,z)
% Locate the time when height passes through zero in a 
% decreasing direction and stop integration.
cond = 1e-5;

global obs;
global cnt;
global gl;
%coeff = 2;
coeff = 2.7;
point = z';
deriv = zeros(2,1);
beta = -1 * ones(cnt + 1);
deltabeta = -1 * ones(cnt + 1, 2);

beta(1) =  2500 - (50 - point(1))^2 - (50 - point(2))^2;
deltabeta(1, 1) = -2 * (point(1) - 50);
deltabeta(1, 2) = -2 * (point(2) - 50);
% for y=1:cnt
%     cur2 = obs{y};
%     deltabeta(1, 1) = deltabeta(1, 1) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%     deltabeta(1, 2) = deltabeta(1, 2) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%     
% end

for x=1:cnt
    current = obs{x};
    beta(x + 1) = - current(3)^2 + (current(1) - point(1))^2 + (current(2) - point(2))^2;
    deltabeta(x + 1, 1) = 2 * (point(1) - current(1));
    deltabeta(x + 1, 2) = 2 * (point(2) - current(2));
    

%     for y=0:cnt
%         if y == x
%             continue;
%         elseif y == 0
%             deltabeta(x + 1, 1) = deltabeta(x + 1, 1) * beta(1);
%             deltabeta(x + 1, 2) = deltabeta(x + 1, 2) * beta(1);
%         else
%             cur2 = obs{y};
%             deltabeta(x + 1, 1) = deltabeta(x + 1, 1) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%             deltabeta(x + 1, 2) = deltabeta(x + 1, 2) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%         end
%     end
end
for x=1:cnt+1 
    for y=1:cnt+1
        if x == y
            continue;
        else
            deltabeta(x, :) = deltabeta(x, :) * beta(y); 
        end
            
    end
    
end
mbeta = beta(1);
sdbeta(1) = deltabeta(1, 1);
sdbeta(2) = deltabeta(1, 2);
for x=1:cnt
    mbeta = mbeta * beta(x + 1);
    sdbeta(1) = sdbeta(1) + deltabeta(x + 1, 1);
    sdbeta(2) = sdbeta(2) + deltabeta(x + 1, 2);
end
Dbeta(1) = sdbeta(1);
Dbeta(2) = sdbeta(2);

beta(1) =  2500 - (50 - point(1))^2 - (50 - point(2))^2;
deltabeta(1, 1) = -2 * (point(1) - 50);
deltabeta(1, 2) = -2 * (point(2) - 50);
% for y=1:cnt
%     cur2 = obs{y};
%     deltabeta(1, 1) = deltabeta(1, 1) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%     deltabeta(1, 2) = deltabeta(1, 2) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%     
% end

for x=1:cnt
    current = obs{x};
    beta(x + 1) = - current(3)^2 + (current(1) - point(1))^2 + (current(2) - point(2))^2;
    deltabeta(x + 1, 1) = 2 * (point(1) - current(1));
    deltabeta(x + 1, 2) = 2 * (point(2) - current(2));
    

%     for y=0:cnt
%         if y == x
%             continue;
%         elseif y == 0
%             deltabeta(x + 1, 1) = deltabeta(x + 1, 1) * beta(1);
%             deltabeta(x + 1, 2) = deltabeta(x + 1, 2) * beta(1);
%         else
%             cur2 = obs{y};
%             deltabeta(x + 1, 1) = deltabeta(x + 1, 1) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%             deltabeta(x + 1, 2) = deltabeta(x + 1, 2) * (((point(1) - cur2(1))^2 + (point(2) - cur2(2))^2) - cur2(3)^2);
%         end
%     end
end
for x=1:cnt+1
    temp = deltabeta(x, :);
    deltabeta(x, :) = [0 0]; 
    for y=1:cnt+1
        if x == y
            continue;
        else
            deltabeta(x, :) = deltabeta(x, :) + temp * beta(y); 
        end
            
    end
    
end

mbeta = beta(1);
sdbeta(1) = deltabeta(1, 1);
sdbeta(2) = deltabeta(1, 2);
for x=1:cnt
    mbeta = mbeta * beta(x + 1);
    sdbeta(1) = sdbeta(1) + deltabeta(x + 1, 1);
    sdbeta(2) = sdbeta(2) + deltabeta(x + 1, 2);
end
Dbeta(1) = sdbeta(1);
Dbeta(2) = sdbeta(2);

Dspec = (1/coeff)*(sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2)^(2*coeff)+mbeta)^(1/coeff-1) * (2* coeff * ((point(1) - gl(1))^2 + (point(2) - gl(2))^2)^(coeff-1)* (point-gl) + Dbeta) ;

deriv =(2* (point-gl) * (sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2)^(2*coeff)+mbeta)^(1/coeff) - ((point(1) - gl(1))^2 + (point(2) - gl(2))^2) * Dspec) / (sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2)^(2*coeff)+mbeta)^(2/coeff);
deriv = deriv';
% A = (point(1) - gl(1))^2 + (point(2) - gl(2))^2;
% Dz(1) = (point(1) - gl(1)) / sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2);
% Dz(2) = (point(2) - gl(2)) / sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2);
% DA(1) = 2 * sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2) * Dz(1);
% DA(2) = 2 * sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2) * Dz(1);
% 
% B = ((gl(1) - point(1))^2 + (gl(2) - point(2))^2)^coeff + mbeta;
% DB(1) = 2 * coeff *  ((gl(1) - point(1))^2 + (gl(2) - point(2))^2)^(coeff-0.5) * Dz(1) + Dbeta(1);
% DB(2) = 2 * coeff *  ((gl(1) - point(1))^2 + (gl(2) - point(2))^2)^(coeff-0.5)* Dz(2) + Dbeta(2);
% 
% 
% deriv(1) = DA(1) * B^(-1/coeff) + A * (-1 / coeff) * B^((-1-coeff)/coeff)* DB(1);
% deriv(2) = DA(2) * B^(-1/coeff) + A * (-1 / coeff) * B^((-1-coeff)/coeff)* DB(2);
% deriv(1) = -1*deriv(1);
% deriv(2) = -1*deriv(2);

sum = [(norm(deriv)>cond) *1  norm(gl-point)<cond] ; % stop when the norm of the gradiant is less than or equal to epsilon    

isterminal = [1 1];   % Stop the integration
direction = [0 0];   % any direction
