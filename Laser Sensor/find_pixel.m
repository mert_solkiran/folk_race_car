function [img,points,points_gauss ] = find_pixel( img )
% A = [ 0 1 2 3 4 25 6 7 8 9 ; 9 5 1 20 3 6 4 7 8 5]'
[M,I] = max(img,[],1);

threshold = 0.2;

[row, column] = size(img);



windowSize = 2;
b = (1/windowSize)*ones(1,windowSize);
a=1;

crop_up = 1;
crop_down = row;
for (i=1:1:row)

    row_vals = img(i,:);
    [row_val,I] = max(row_vals);
    if (row_val < threshold)
    
        crop_up = i;
    else
        break;
    end
end

for (i=row:-1:1)

    row_vals = img(i,:);
    [row_val,I] = max(row_vals);
    if (row_val < threshold)
    
        crop_down = i;
    else
        break;
    end
end

img = img(crop_up:crop_down,:);
[row, column] = size(img);
% width = ceil(row/20);
width = 7;
img = img(0+width:row-width,:);
points = zeros(2,column);
img = imresize(img,[row*5, column*5]);
[row, column] = size(img);
for (i=1:1:column)

    col_vals = img(:,i);
%     col_vals_filtered = col_vals;
%     col_vals_filtered = filter(b,a,col_vals);
%     col_vals_filtered = flip(filter(b,a,flip(col_vals_filtered)));
    [row_val,I] = max(col_vals);
%     points_gauss(1,i) = find_peak( col_vals_filtered, width );
    if (row_val < threshold/2)
    
        I = NaN;
    
    end
    points_gauss(1,i) = I;
    points_gauss(2,i) = i;
    points(1,i) = I;
    points(2,i) = i;
    
%     img_out(I(i),i) = 0;

end


end

