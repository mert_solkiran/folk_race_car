function [final_matrix] = basic_laser_model(origin_of_laser, rotation_matrix)
front_side_width = 1200;
back_side_width = 56.5;
depth = 1058;
stand_off = 32.5;

total_beam = 40; % total laser beams to be generated;


p1 = [stand_off; (back_side_width/2)];
p2 = [stand_off+depth;(front_side_width/2)];
p3 = p2.*[1;-1];
p4 = p1.*[1;-1];

ext_lp = [p1,p2,p3,p4,p1]; %laser points that describe borders of laser sensor range

back_side_points = linspace(p1,p4,total_beam);
front_side_points = linspace(p2,p3,total_beam);

final_matrix = [front_side_points;back_side_points];


%figure
%hold on
%plotting_matrix = [];
%for counter=1:total_beam
%  
%plotting_matrix = [ final_matrix, [[back_side_points(1,counter),front_side_points(1,counter)];[back_side_points(2,counter),front_side_points(2,counter)]]];
%
%
%endfor

final_matrix(1:2,:) = (rotation_matrix*(final_matrix(1:2,:)+origin_of_laser));
final_matrix(3:4,:) = (rotation_matrix*(final_matrix(3:4,:)+origin_of_laser));
%plot(final_matrix(1,:),final_matrix(2,:));
endfunction