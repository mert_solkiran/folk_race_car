function [ plane ] = laser(depth)

format long

laser_angle = 120; % laser line angle in degrees


p1 = [0;0;0]; % origin point
p2 = p1; % left point of triangle
p3 = p1; % right point of triangle


p2(1) = p1(1) + depth * tand(laser_angle/2);
p2(2) = depth;
p2(3) = 0;
% coordinates of left point calculated

p3(1) = p1(1) - depth * tand(laser_angle/2);
p3(2) = depth;
p3(3) = 0;
% coordinates of right point calculated

plane = [p1,p2,p3, p1];

