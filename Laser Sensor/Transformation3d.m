function [ output_matrix ] = Transformation3d( rot_matrix, translation_matrix, input_matrix )
%UNT�TLED3 Summary of this function goes here
%   Detailed explanation goes here

rx = rot_matrix(1);
ry = rot_matrix(2);
rz = rot_matrix(3);

x_matrix = [1 0 0; 0 cosd(rx) -sind(rx); 0 sind(rx) cosd(rx)];

y_matrix = [cosd(ry) 0 sind(ry); 0 1 0; -sind(ry) 0 cosd(ry) ];

z_matrix = [cosd(rz) -sind(rz) 0; sind(rz) cosd(rz) 0; 0 0 1];



[row_of_input, column_of_input] = size(input_matrix);


n_translation_matrix = input_matrix.*0;

for (i=1:1:column_of_input)

    n_translation_matrix(:,i) = translation_matrix;

end

translation_matrix = n_translation_matrix;

output_matrix = input_matrix + translation_matrix;

rotation_matrix = x_matrix * y_matrix * z_matrix;

output_matrix =  rotation_matrix * output_matrix;

end

