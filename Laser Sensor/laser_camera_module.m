clear
clc
close all

rot_point_laser_offset_z = 100;
rot_point_laser_offset_x = 0;
rot_point_laser_offset_y = -20;
rot_angle = 40;
rot_point_camera_offset = 0;

laser_depth = 10; % depth of laser line in mm
camera_view_depth = 10; % depth of view, mm
[plane_of_cam_1, plane_of_cam_2, plane_of_cam_3, plane_of_cam_4, cam_org, lines_of_cam, HRES, VRES] = camera(camera_view_depth);
[plane_of_laser] = laser(laser_depth);

%% STANDART ROTATIONS

rotate_z = 0;
rotate_y = 0;
rotate_x = -90;
rot_org = [0,0,0];
lines_of_cam = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, lines_of_cam);
plane_of_cam_1 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_1);
plane_of_cam_2 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_2);
plane_of_cam_3 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_3);
plane_of_cam_4 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_4);

%% STANDART ROTATIONS


% laser offset
offset_z = rot_point_laser_offset_z;
offset_y = rot_point_laser_offset_y;
plane_of_laser = Transformation3d([0;0;0], [0;offset_y;offset_z], plane_of_laser);

% camera rotation
rotate_x = rot_angle;
rot_org = [0,0,0];
lines_of_cam = Transformation3d([rotate_x;0;0], rot_org, lines_of_cam);
plane_of_cam_1 = Transformation3d([rotate_x;0;0], rot_org, plane_of_cam_1);
plane_of_cam_2 = Transformation3d([rotate_x;0;0], rot_org, plane_of_cam_2);
plane_of_cam_3 = Transformation3d([rotate_x;0;0], rot_org, plane_of_cam_3);
plane_of_cam_4 = Transformation3d([rotate_x;0;0], rot_org, plane_of_cam_4);

% camera offset
offset_z = rot_point_camera_offset;
lines_of_cam = Transformation3d([0;0;0], [0;0;offset_z], lines_of_cam);
plane_of_cam_1 = Transformation3d([0;0;0], [0;0;offset_z], plane_of_cam_1);
plane_of_cam_2 = Transformation3d([0;0;0], [0;0;offset_z], plane_of_cam_2);
plane_of_cam_3 = Transformation3d([0;0;0], [0;0;offset_z], plane_of_cam_3);
plane_of_cam_4 = Transformation3d([0;0;0], [0;0;offset_z], plane_of_cam_4);
disp("Plane 1")
disp(plane_of_cam_1)
disp("Plane 2")
disp(plane_of_cam_2)
disp("Plane 3")
disp(plane_of_cam_3)
disp("Plane 4")
disp(plane_of_cam_4)
figure(1)
hold on
plot3(plane_of_cam_1(1,:), plane_of_cam_1(2,:), plane_of_cam_1(3,:), 'b')
figure(2)
hold on
plot3(plane_of_cam_2(1,:), plane_of_cam_2(2,:), plane_of_cam_2(3,:), 'b')
figure(3)
hold on
plot3(plane_of_cam_3(1,:), plane_of_cam_3(2,:), plane_of_cam_3(3,:), 'b')
figure(4)
hold on
plot3(plane_of_cam_4(1,:), plane_of_cam_4(2,:), plane_of_cam_4(3,:), 'b')
% rotate_z = 0;
% rotate_y = 90;
% rotate_x = 0;
% rot_org = [0,0,0];
% lines_of_cam = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, lines_of_cam);
% plane_of_cam_1 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_1);
% plane_of_cam_2 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_2);
% plane_of_cam_3 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_3);
% plane_of_cam_4 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_4);

% rotate_z = 0;
% rotate_y = 90;
% rotate_x = 0;
% rot_org = [0,0,0];
% lines_of_cam = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, lines_of_cam);
% plane_of_cam_1 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_1);
% plane_of_cam_2 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_2);
% plane_of_cam_3 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_3);
% plane_of_cam_4 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_4);

[intersection_1,check] = plane_line_intersect(calculate_normal_of_plane(plane_of_laser),plane_of_laser(:,1),lines_of_cam(:,1),lines_of_cam(:,2));
[intersection_2,check] = plane_line_intersect(calculate_normal_of_plane(plane_of_laser),plane_of_laser(:,1),lines_of_cam(:,1),lines_of_cam(:,3));
[intersection_3,check] = plane_line_intersect(calculate_normal_of_plane(plane_of_laser),plane_of_laser(:,1),lines_of_cam(:,1),lines_of_cam(:,4));
[intersection_4,check] = plane_line_intersect(calculate_normal_of_plane(plane_of_laser),plane_of_laser(:,1),lines_of_cam(:,1),lines_of_cam(:,5));
% plane_of_laser_1 = Rotation3d([0;-30;0], [0;0;0], plane_of_laser);



%% section plot
% fill3(plane_of_cam_1(1,:),plane_of_cam_1(2,:),plane_of_cam_1(3,:),'r',plane_of_cam_2(1,:),plane_of_cam_2(2,:),plane_of_cam_2(3,:),'r',plane_of_cam_3(1,:),plane_of_cam_3(2,:),plane_of_cam_3(3,:),'b',plane_of_cam_4(1,:),plane_of_cam_4(2,:),plane_of_cam_4(3,:),'b',plane_of_laser(1,:),plane_of_laser(2,:),plane_of_laser(3,:),'g',plane_of_laser_1(1,:),plane_of_laser_1(2,:),plane_of_laser_1(3,:),'y')
figure
hold on
plot3(plane_of_cam_1(1,:),plane_of_cam_1(2,:),plane_of_cam_1(3,:),'r',plane_of_cam_2(1,:),plane_of_cam_2(2,:),plane_of_cam_2(3,:),'r',plane_of_cam_3(1,:),plane_of_cam_3(2,:),plane_of_cam_3(3,:),'b',plane_of_cam_4(1,:),plane_of_cam_4(2,:),plane_of_cam_4(3,:),'b',plane_of_laser(1,:),plane_of_laser(2,:),plane_of_laser(3,:),'y')
% axis([-3000,3000,-3000,3000,-3000,3000])
xlabel('x') % x-axis label
ylabel('y') % x-axis label
zlabel('z') % x-axis label
grid on
axis equal 
%alpha(0.5)
%% end of cell plot

scatter3(intersection_1(1,:),intersection_1(2,:),intersection_1(3,:)); 
scatter3(intersection_2(1,:),intersection_2(2,:),intersection_2(3,:));
scatter3(intersection_3(1,:),intersection_3(2,:),intersection_3(3,:));
scatter3(intersection_4(1,:),intersection_4(2,:),intersection_4(3,:));
x_axis_top = (sum((intersection_2 - intersection_1).^2)).^0.5;
x_axis_bottom = (sum((intersection_3 - intersection_4).^2)).^0.5;
delta_x = abs(x_axis_bottom - x_axis_top);
mid_point_of_1_2 = (intersection_2 + intersection_1)./2;
mid_point_of_3_4 = (intersection_3 + intersection_4)./2;
scatter3(mid_point_of_1_2(1,:),mid_point_of_1_2(2,:),mid_point_of_1_2(3,:));
scatter3(mid_point_of_3_4(1,:),mid_point_of_3_4(2,:),mid_point_of_3_4(3,:));
z_axis = (sum((mid_point_of_1_2 - mid_point_of_3_4).^2)).^0.5;
angle_of_system = atand((delta_x/2)/z_axis)*2;
standoff = (sum((mid_point_of_3_4 - [0;0;rot_point_laser_offset_z]).^2)).^0.5; 
dist_mid_top_to_org = (sum(mid_point_of_3_4.^2))^0.5;
dist_mid_bottom_to_org = (sum(mid_point_of_1_2.^2))^0.5;
x_res = x_axis_top / HRES;
z_res = z_axis / VRES;
X = ['X axis Top:',num2str(x_axis_top)];
disp(X)
X = ['X axis Bottom:',num2str(x_axis_bottom)];
disp(X)
X = ['X axis Difference:',num2str(delta_x)];
disp(X)
X = ['X axis Res:',num2str(x_res)];
disp(X)
X = ['Z axis:',num2str(z_axis)];
disp(X)
X = ['Z axis Res:',num2str(z_res)];
disp(X)
X = ['Standoff:',num2str(standoff)];
disp(X)
X = ['Angle:',num2str(angle_of_system)];
disp(X)
X = ['Distance from bottom to origin:',num2str(dist_mid_bottom_to_org)];
disp(X)
X = ['Distance from Top to origin:',num2str(dist_mid_top_to_org)];
disp(X)

% scatter3(intersection_3(1,:),intersection_3(2,:),intersection_3(3,:)); 
% scatter3(intersection_4(1,:),intersection_4(2,:),intersection_4(3,:));


