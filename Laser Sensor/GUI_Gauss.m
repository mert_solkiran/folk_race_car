

function varargout = GUI_Gauss(varargin)
% GUI_GAUSS MATLAB code for GUI_Gauss.fig
%      GUI_GAUSS, by itself, creates a new GUI_GAUSS or raises the existing
%      singleton*.
%
%      H = GUI_GAUSS returns the handle to a new GUI_GAUSS or the handle to
%      the existing singleton*.
%
%      GUI_GAUSS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_GAUSS.M with the given input arguments.
%
%      GUI_GAUSS('Property','Value',...) creates a new GUI_GAUSS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_Gauss_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_Gauss_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_Gauss

% Last Modified by GUIDE v2.5 29-Nov-2015 09:14:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_Gauss_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_Gauss_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end
function [] = gauss_calculator(hObject, eventdata)
    
handles = guidata(hObject);


slider_2_val = floor(get(handles.slider2,'Value'));
slider_3_val = floor(get(handles.slider3,'Value'));
slider_4_val = floor(get(handles.slider4,'Value'));
slider_5_val = double(get(handles.slider5,'Value'));

textLabel_4 = sprintf('Gauss Kernel= %f', slider_2_val);
textLabel_5 = sprintf('Median Kernel = %f', slider_3_val);
textLabel_7 = sprintf('Erode Size = %f', slider_4_val);
textLabel_8 = sprintf('Erode Size = %f', slider_5_val);

set(handles.text4, 'string', textLabel_4);
set(handles.text5, 'string', textLabel_5);
set(handles.text7, 'string', textLabel_7);
set(handles.text8, 'string', textLabel_8);

gray_image = handles.gray_image;

if (slider_2_val ~=0)
    handles.gauss_filtered_image = imgaussfilt(gray_image,slider_2_val);
else
    handles.gauss_filtered_image = gray_image;
end
if (slider_3_val ~=0)
    median_width = ceil(slider_3_val/3);
    handles.gauss_filtered_image = medfilt2(handles.gauss_filtered_image,[slider_3_val, median_width]);
end
if (slider_4_val ~=0)
    se = strel('disk',slider_4_val);
    handles.gauss_filtered_image = imerode(handles.gauss_filtered_image,se);
else

end

if (slider_5_val > 0)
    handles.gauss_filtered_image(handles.gauss_filtered_image<slider_5_val) = 0;
else

end

guidata(hObject,handles);

[rows, columns] = size(handles.gauss_filtered_image);
[A, Index] = max(handles.gauss_filtered_image);
pos_matrix = zeros(columns,2);
pos_matrix(:,2) = Index;
pos_columns = 1:1:columns;
pos_matrix(:,1) = pos_columns;
handles.gauss_filtered_image = insertMarker(handles.gauss_filtered_image, pos_matrix,'*', 'color', 'red', 'size', 1);
h = imshow(handles.gauss_filtered_image, 'Parent', handles.axes2);

end
% --- Executes just before GUI_Gauss is made visible.
function GUI_Gauss_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_Gauss (see VARARGIN)

% Choose default command line output for GUI_Gauss
clc
handles.output = hObject;
% set(SliderHandle, 'Min', 3)
% set(SliderHandle, 'Max', 20)
% Update handles structure
guidata(hObject, handles);
[FileName,iscancelled] = imgetfile();

if (iscancelled)
     FileName = 'gauss_test_image.jpg';
end

rgb_image = im2double(imread(FileName));
handles.gray_image = rgb_image(:,:,3);
[row,column] = size(handles.gray_image);
scale = 200/row;
handles.gray_image = imresize(handles.gray_image,scale);
% imshow(handles.gray_image, 'Parent', handles.axes2);
guidata(hObject,handles);
set(handles.text5, 'String', 'Image Read');
gauss_calculator(hObject,eventdata);



% UIWAIT makes GUI_Gauss wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end
% --- Outputs from this function are returned to the command line.
function varargout = GUI_Gauss_OutputFcn(hObject, eventdata, handles) 

% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get default command line output from handles structure
varargout{1} = handles.output;

end

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% --- Executes during object creation, after setting all properties.
end
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
    

end

addlistener(hObject,'ContinuousValueChange',@gauss_calculator);
end
% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% --- Executes during object creation, after setting all properties.
end
function slider3_CreateFcn(hObject, eventdata, handles)


% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);

end

addlistener(hObject,'ContinuousValueChange',@gauss_calculator);
end


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

end
% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

addlistener(hObject,'ContinuousValueChange',@gauss_calculator);

end


% --- Executes on slider movement.
function slider5_Callback(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

end
% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
addlistener(hObject,'ContinuousValueChange',@gauss_calculator);
end
