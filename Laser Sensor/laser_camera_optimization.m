clear
clc
close all


laser_min_offset_z = 10;
laser_max_offset_z = 100;
laser_step_offset_z = 5;
laser_min_rot_angle = 24;
laser_max_rot_angle = 27;
laser_step_rot_angle = 0.5;

laser_offset_z = 100;
laser_rot_angle = 25;
laser_offset_y = -20;

output_matrix = [];
disp("Calculation started")
for laser_offset_z = laser_min_offset_z:laser_step_offset_z:laser_max_offset_z
  for laser_rot_angle = laser_min_rot_angle:laser_step_rot_angle:laser_max_rot_angle

    [x_axis_top, x_axis_bottom, x_res, z_axis, z_res, standoff, angle_of_system]=laser_camera_module_v2(laser_offset_z, laser_offset_y, laser_rot_angle);
    output_matrix = [output_matrix;laser_offset_z,laser_rot_angle, x_axis_top, x_axis_bottom, x_res, z_axis, z_res, standoff, angle_of_system];
  endfor
endfor
date_time = strftime ("%Y_%m_%d_%H_%M_%S", localtime (time ()))
disp("Writing matrix to file......")
file_name = ["Output_matrix_",date_time,".txt"]
csvwrite(file_name, output_matrix)
disp("File written.")

%X = ['X axis Top:',num2str(x_axis_top)];
%disp(X)
%X = ['X axis Bottom:',num2str(x_axis_bottom)];
%disp(X)
%%X = ['X axis Difference:',num2str(delta_x)];
%%disp(X)
%X = ['X axis Res:',num2str(x_res)];
%disp(X)
%X = ['Z axis:',num2str(z_axis)];
%disp(X)
%X = ['Z axis Res:',num2str(z_res)];
%disp(X)
%X = ['Standoff:',num2str(standoff)];
%disp(X)
%X = ['Angle:',num2str(angle_of_system)];
%disp(X)
%%X = ['Distance from bottom to origin:',num2str(dist_mid_bottom_to_org)];
%%disp(X)
%%X = ['Distance from Top to origin:',num2str(dist_mid_top_to_org)];
%disp(X)