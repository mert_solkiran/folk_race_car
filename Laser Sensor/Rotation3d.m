function [ output_matrix ] = Rotation3d( rot_matrix, org_matrix, input_matrix )
%UNT�TLED3 Summary of this function goes here
%   Detailed explanation goes here

rx = rot_matrix(1);
ry = rot_matrix(2);
rz = rot_matrix(3);

ox = org_matrix(1);
oy = org_matrix(2);
oz = org_matrix(3);

% input_matrix = input_matrix - org_matrix;

x_matrix = [1 0 0; 0 cosd(rx) -sind(rx); 0 sind(rx) cosd(rx)];

y_matrix = [cosd(ry) 0 sind(ry); 0 1 0; -sind(ry) 0 cosd(ry) ];

z_matrix = [cosd(rz) -sind(rz) 0; sind(rz) cosd(rz) 0; 0 0 1];

output_matrix = input_matrix * x_matrix * y_matrix * z_matrix;

% output_matrix = output_matrix + org_matrix;
end

