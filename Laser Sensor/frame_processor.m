clear
clc
close all

dirName = 'H:\Disaridan isler\1512\Lazer Sens�r\M�hendislik\Matlab Code\test_img';
files = [dir(fullfile(dirName,'*.jpg')); dir(fullfile(dirName,'*.bmp')); dir(fullfile(dirName,'*.jpeg'))];
file_names_cell = {files.name};
file_names = cell2mat(file_names_cell);
sample_size = length(file_names_cell);

inspection_x = 1400;

for (ctr_1=1:1:sample_size)
str = sprintf('Processing %d file: %s',ctr_1, file_names_cell{ctr_1});
display(str);
[rgb_image, map] = (imread(fullfile(dirName,file_names_cell{ctr_1})));
rgb_image = im2double(rgb_image);
% rgb_image(:,:,1) = rgb_image(:,:,1) .* 0;
% rgb_image(:,:,2) = rgb_image(:,:,2) .* 0;
% rgb_image(:,:,3) = rgb_image(:,:,3) .* 1.2;


[ height, width, rgb ] = size(rgb_image);


ycbcr_image = rgb2ycbcr(rgb_image);

cb_comp = mat2gray(ycbcr_image(:,:,2));
cb_comp = rgb2gray(rgb_image);

figure
imshow(mat2gray(cb_comp));

filter_Size = round(height / 200);

cb_comp = imgaussfilt(cb_comp,filter_Size);


[cb_comp,points,points_gauss] = find_pixel(cb_comp);

figure
imshow(mat2gray(cb_comp));
title('Processed Image');
hold on
plot(points(2,:),points(1,:),'.','Color','r','markersize',2)
plot(points_gauss(2,:),points_gauss(1,:),'.','Color','b','markersize',2)
hold off
% figure
% surf(imresize(filtered_image,0.1))
% caxis([0 1])

end