function [ normal_of_plane ] = calculate_normal_of_plane( points )

p1 = points(:,1);
p2 = points(:,2);
p3 = points(:,3);

normal_of_plane = cross(p1'-p2', p1'-p3');
normal_of_plane = normal_of_plane / norm( normal_of_plane ); % just to make it unit length

end

