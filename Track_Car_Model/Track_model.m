clear
clc
close all

step = 1; %mm
F1_x_end = 1000; %quantity of step
F2_x_start = F1_x_end; %quantity of step
F2_x_end = 3500-1; %quantity of step
F3_x_start = F2_x_end;
F3_x_end = 10500-1;
F4_x_start = F3_x_end;
F4_x_end = 12000-1;


left_wall_offset = 800;
left_corner_radius = 2000;
right_corner_radius = 2000;

x_matrix = 0:step:(step*F4_x_end);
left_wall_y_matrix = zeros(1,length(x_matrix));
right_wall_y_matrix = zeros(1,length(x_matrix));

left_wall_modified_x_points = acos(x_matrix(1,1:F2_x_end-F2_x_start+1)/left_corner_radius);
left_wall_y_matrix(1,F2_x_start:F2_x_end) = real((sin(left_wall_modified_x_points)*left_corner_radius-left_corner_radius)*-1);
left_wall_y_matrix(1,F3_x_start:F3_x_end+1) = left_corner_radius;
left_wall_y_matrix(1,F4_x_start:F4_x_end+1) = 0;

right_wall_modified_x_points = acos(x_matrix(1,1:F2_x_end-F2_x_start+1)/right_corner_radius);
right_wall_y_matrix(1,F2_x_start:F2_x_end) = real((sin(right_wall_modified_x_points)*right_corner_radius-right_corner_radius)*-1);
right_wall_y_matrix(1,F3_x_start:F3_x_end+1) = right_corner_radius;


% shift the right wall matrix to create better corners
shifting_amount = (left_wall_offset + right_corner_radius - left_corner_radius); 
right_wall_y_matrix = [zeros(1,shifting_amount),right_wall_y_matrix];
right_wall_y_matrix = right_wall_y_matrix(1,1:F4_x_end-shifting_amount);

right_wall_y_matrix(1,F4_x_start-left_wall_offset:F4_x_end+1) = 0;

left_wall_y_matrix = left_wall_y_matrix + left_wall_offset; % add some offset to left wall so functions will look like a track

plot(x_matrix,left_wall_y_matrix,"linewidth",7,right_wall_y_matrix,"linewidth",7)
axis ( "equal" );
xlim([-50, F4_x_end*step]);
ylim([-50, max(left_wall_y_matrix)+100]);