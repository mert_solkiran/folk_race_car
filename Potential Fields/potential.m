function [x y time area rarea isField] = potential(obstacles, count, size, goal, start)
global obs;
global gl;
global qs;
global maxgra;
global coeff2;
global coeff;
global sz;
global cnt;
global ds;
global point;
point = start;
ds=50; 
cnt = count;
sz = size;
qs = 10; 
obs = obstacles;
gl = goal;
coeff=10;
%for graph purposes
[area, gradient, force, isField] = attraction(size, goal, ds, coeff);

maxi = max(max(area));
maxgra = sqrt(max(max(gradient(:,:,1)^2 + gradient(:,:,2)^2)));
coeff2 = 5.2 * maxi;
%for graph purposes
[rarea, rgradient] = rebellion(qs, obstacles, count, maxi, maxgra, coeff, coeff2, size);



refine = 4;
options = odeset('Events',@stopCondition,'RelTol',1e-44,'AbsTol',1e-46,'Refine',refine);
tic;
[time,axises,te,ye,ie] = ode45(@potentialg, [0 1e1], start, options);
zet = toc;
zet = 0;
x=axises(:,1);
y=axises(:,2);




function deriv = potentialg(t,z)
global obs;
global gl;
global qs;
global ds;
global maxgra;
global coeff2;
global coeff;
global sz;
global cnt;
point = z';
deriv = zeros(2,1);
grad = [-1 -1];
gradient = [1 1];
for x=1:cnt
    current = obs{x};
    
    distance = sqrt((point(1) - current(1))^2 + (point(2) - current(2))^2) - current(3);
    if distance <= 0
        grad = [0 0];
        break;
    elseif distance < qs
        grad(1) = grad(1) + coeff2 * (1/qs - 1/distance) / distance^2 * (point(1) - current(1));
        grad(2) = grad(2) + coeff2 * (1/qs - 1/distance) / distance^2 * (point(2) - current(2));
    end
end

sum = sqrt(grad(1)^2 + grad(2)^2);
if sum  > maxgra
    grad(1) = (grad(1) / sum) * maxgra;
    grad(2) = (grad(2) / sum) * maxgra;
end

distance = sqrt((point(1) - gl(1))^2 + (point(1) - gl(2))^2);

if distance <= ds
    gradient(1) = coeff * (point(1) - gl(1));
    gradient(2) = coeff *(point(2) - gl(2));
else
    gradient(1) = coeff * ds *  (point(1) - gl(1)) / distance;
    gradient(2) = coeff * ds *  (point(2) - gl(2)) / distance;
end


deriv(1) = -1 * (grad(1) + gradient(1));
deriv(2) = -1 * (grad(2) + gradient(2));


function [sum,isterminal,direction] = stopCondition(t,z)
cond = 3;

global obs;
global gl;
global qs;
global ds;
global maxgra;
global coeff2;
global coeff;
global sz;
global cnt;
global point;
deriv = zeros(2,1);
grad = [-1 -1];
gradient = [1 1];
point = z';
for x=1:cnt
    current = obs{x};
    
    distance = sqrt((point(1) - current(1))^2 + (point(2) - current(2))^2) - current(3);
    if distance <= 0
        grad = [0 0];
        break;
    elseif distance < qs
        grad(1) = grad(1) + coeff2 * (1/qs - 1/distance) / distance^2 * (point(1) - current(1));
        grad(2) = grad(2) + coeff2 * (1/qs - 1/distance) / distance^2 * (point(2) - current(2));
    end
end

sum = sqrt(grad(1)^2 + grad(2)^2);
if sum  > maxgra
    grad(1) = (grad(1) / sum) * maxgra;
    grad(2) = (grad(2) / sum) * maxgra;
end

distance = sqrt((point(1) - gl(1))^2 + (point(2) - gl(2))^2);
    
if distance <= ds
    gradient(1) = coeff * (point(1) - gl(1));
    gradient(2) = coeff *(point(2) - gl(2));
else
    gradient(1) =  coeff * ds *  (point(1) - gl(1)) / distance;
    gradient(2) =  coeff * ds *  (point(2) - gl(2)) / distance;
end


deriv(1) = grad(1) + gradient(1);
deriv(2) = grad(2) + gradient(2);
sum = sqrt(deriv(1)^2 + deriv(2)^2);
if sum < cond 
   sum = 0;
end

isterminal = 1;   
direction = 0;   
