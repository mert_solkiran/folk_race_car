function [ellipse_1, ellipse_2] = Track_model_v2()
  a_of_ellipse = 6000;
  b_of_ellipse = 1000;


  a_of_ellipse_2 = 6750;
  b_of_ellipse_2 = 1750;

  x_points = 0:1:a_of_ellipse;
  x_points_2 = 0:1:a_of_ellipse_2;
  y_1 = (b_of_ellipse/a_of_ellipse) * sqrt(a_of_ellipse^2-x_points.^2);
  y_2 = -1*(b_of_ellipse/a_of_ellipse) * sqrt(a_of_ellipse^2-x_points.^2);
  y = [y_1,flip(y_2)];
  y = [y,flip(y)];
  x = [x_points,flip(x_points)];
  x = [x,flip(-1*x)];

  y_1 = (b_of_ellipse_2/a_of_ellipse_2) * sqrt(a_of_ellipse_2^2-x_points_2.^2);
  y_2 = -1*(b_of_ellipse_2/a_of_ellipse_2) * sqrt(a_of_ellipse_2^2-x_points_2.^2);
  ellipse_2_y = [y_1,flip(y_2)];
  ellipse_2_y = [ellipse_2_y,flip(ellipse_2_y)];
  x_2 = [x_points_2,flip(x_points_2)];
  x_2 = [x_2,flip(-1*x_2)];
  ellipse_1 = [x; y];
  ellipse_2 = [x_2; ellipse_2_y];

  ellipse_1 = [ellipse_1,ellipse_1(:,1)];
  ellipse_2 = [ellipse_2,ellipse_2(:,1)];
%  plot(x,y,"linewidth",7,x_2,ellipse_2_y,"linewidth",7)
%  axis ( "equal" );
%  xlim([-50, F4_x_end*step]);
%  ylim([-50, max(left_wall_y_matrix)+100]);
end