function [area] = navigation(obstacles, count, coeff, size)
global gl;

area = -1 * ones(size, size);
for z = 1:size
    for y = 1:size
        area(z,y) = 0;
        
        dist = ((z - gl(1))^2 + (y - gl(2))^2)^coeff;
        
        beta = - (z -50)^2 + (y - 50)^2  + 2500;
        
        for k=1:count
            obs = obstacles{k};
            beta = beta * ((z - obs(1))^2 + (y - obs(2))^2  - obs(3)^2);
        end
        
        if beta == 0
            beta = 3;
        end
        
        area(z,y) = real((dist / (beta + dist))^(1/coeff));

    end
end
        