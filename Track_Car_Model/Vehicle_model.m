function [sharp_sen_arr_pnts, corner_points, origin_of_laser , laser_matrix, sharp_distance_matrix, laser_intersection_matrix, sharp_intersection_matrix, laser_measurement_matrix, yaw_angle, yaw_rate, steering_angle, velocity, velocity_angle, wheel_velocity, velocity_angle_front, velocity_angle_rear, acc_x, acc_y, vel_x, vel_y] = Vehicle_model(origin_of_vehicle, Ellipse_track_1, Ellipse_track_2, yaw_angle, yaw_rate, steering_angle, velocity, velocity_angle, desired_steering_angle, desired_acceleration, sampling_interval, wheel_velocity, velocity_angle_front, velocity_angle_rear, acc_x, acc_y, vel_x, vel_y)

weight = 1.7; %Kg
tire_diameter = 36; % mm
tire_radius = 18; % mm
max_wheel_torque = 81; % N/mm for each wheel (two wheeled bicycle model)
wheel_inertia = 0.040; % N / mm^3
tire_cornering_stiffness = 1.23; % www.mas.bg.ac.rs/_media/istrazivanje/fme/vol41/1/08_gvorotovic.pdf
Inertia = 8854;
origin_of_laser = [100;0];
vehicle_length = 200; %mm
vehicle_width = 150; %mm
lf = 80; %distance between cog and front of vehicle
lr = 120; % distance between cog and rear of vehicle
sharp_sensor_range = 500; %mm
max_servo_angle = 180;
servo_speed = 0.035; %sec/60 deg
max_vehicle_velocity = 7000; %mm/s
max_steering_angle = 90;
Steering_controller_Kp = 3;
yaw_rate = [yaw_rate,((yaw_angle(end) - yaw_angle(end-1)) / sampling_interval)]; % yaw rate of vehicle

wheel_torque = max_wheel_torque * desired_acceleration/100;

wheel_velocity = [wheel_velocity, (wheel_velocity(end-1) + ((wheel_torque/wheel_inertia) * sampling_interval))]; % rad/s
wheel_longitudinal_slip = 0;
if (desired_acceleration > 0 ) % acceleration
  wheel_longitudinal_slip = ((tire_radius*wheel_velocity)-wheel_velocity(end))/(tire_radius*wheel_velocity);
  else %braking
  wheel_longitudinal_slip = ((tire_radius*wheel_velocity)-wheel_velocity(end))/(wheel_velocity(end));
 endif
  
wheel_force = (wheel_longitudinal_slip*max_wheel_torque/tire_radius)/0.1; % Newton
##if (wheel_force >=((0.1*max_wheel_torque/tire_radius)/0.1)) % wheel slip thresholds
##  
##  wheel_force = (((0.1*max_wheel_torque/tire_radius)/0.1));
##elseif (wheel_force <=((-0.1*max_wheel_torque/tire_radius)/0.1))
##  wheel_force = ((-0.1*0.1*max_wheel_torque/tire_radius)/0.1));
##  endif
  
  
Fxf = wheel_force*cos(steering_angle);
Fxr = wheel_force;
Fyf = (tire_cornering_stiffness*(steering_angle-velocity_angle_front)) + wheel_force*sin(steering_angle);
Fyr = tire_cornering_stiffness*(-1*velocity_angle_rear);
acc_x = [acc_x, ((Fxr+Fxf)/weight)]; % acceleration along x axis of vehicle
acc_y = [acc_y, ((Fyr+Fyf)/weight)-(vel_x(end)*yaw_rate(end))]; % acceleration along y axis of vehicle
vel_x = [vel_x, (vel_x(end)+(acc_x*sampling_interval))]; % velocity along x axis of vehicle
vel_y = [vel_y, (vel_y(end)+(acc_y*sampling_interval))]; % velocity along y axis of vehicle

velocity_angle_front = atan((vel_y(end)+(lf*yaw_rate(end)))/vel_x(end));
velocity_angle_rear = atan((vel_y(end)-(lr*yaw_rate(end)))/vel_x(end));

yaw_rate_der = ((lf*Fyf)-(lr*Fyr))/Inertia; % derivation of yaw rate
yaw_rate = [yaw_rate, (yaw_rate(end) + (yaw_rate_der*sampling_interval))];
yaw_angle = [yaw_angle, (yaw_angle(end) + (yaw_rate(end)*sampling_interval))];


% we dont know steering ratio but we know max servo angles and max steering angles, so we are going to convert max servo speed to max steering speed
% datasheet: https://www.hpiracing.com/en/part/120018
% https://hobbyking.com/en_us/rjx-fs-0391thv-metal-gear-digital-tail-servo-ultra-high-speed-10-6kg-0-03sec-50g.html
servo_speed = servo_speed/60; % sec/deg
servo_speed = 1/servo_speed; % deg/sec
servo_speed = deg2rad(servo_speed); % rad/sec
max_servo_angle = deg2rad(max_servo_angle)/2; % +/- radian maximum angle
max_steering_angle = deg2rad(max_steering_angle)/2; % +/- radian maximum angle
max_steering_ang_vel = max_steering_angle/(max_servo_angle/servo_speed);% radian/sec maximum steering velocity


half_veh_length = vehicle_length / 2;
half_veh_width = vehicle_width / 2;

steering_error = desired_steering_angle - steering_angle;

% Steering velocity and angle will be compared to datasheet values to create better model of the actuator
steering_velocity = (Steering_controller_Kp*steering_error/sampling_interval);
if (steering_velocity>max_steering_ang_vel)
  steering_velocity = max_steering_ang_vel;
elseif (steering_velocity<(-1*max_steering_ang_vel))
  steering_velocity = -1*max_steering_ang_vel;
  endif
  
  
steering_angle = steering_angle + (steering_velocity*sampling_interval)/2;

if (steering_angle>max_steering_angle)
  steering_angle = max_steering_angle;
elseif (steering_angle<(-1*max_steering_angle))
  steering_angle = -1*max_steering_angle;
  endif

rotation_matrix = [cos(yaw_angle(end)) -sin(yaw_angle(end)); sin(yaw_angle(end)) cos(yaw_angle(end))];













corner_points_1=origin_of_vehicle + [half_veh_length;half_veh_width];
corner_points_2=origin_of_vehicle + [half_veh_length;-half_veh_width];
corner_points_3=origin_of_vehicle + [-half_veh_length;-half_veh_width];
corner_points_4=origin_of_vehicle + [-half_veh_length;half_veh_width];
corner_points=[corner_points_1,corner_points_2,corner_points_3,corner_points_4,corner_points_1];

% Model for Sharp GP2Y0E02A 

sharp_sensor_1_p1 = (corner_points_1-[10;0]);
sharp_sensor_1_p2 = [0;sharp_sensor_range] + sharp_sensor_1_p1;
sharp_sensor_1=[sharp_sensor_1_p1;sharp_sensor_1_p2];

sharp_sensor_2_p1 = (corner_points_2-[10;0]);
sharp_sensor_2_p2 = [0;-sharp_sensor_range] + sharp_sensor_2_p1;
sharp_sensor_2=[sharp_sensor_2_p1;sharp_sensor_2_p2];

sharp_sensor_3_p1 = (corner_points_3+[10;0]);
sharp_sensor_3_p2 = [0;-sharp_sensor_range] + sharp_sensor_3_p1;
sharp_sensor_3=[sharp_sensor_3_p1;sharp_sensor_3_p2];

sharp_sensor_4_p1 = (corner_points_4+[10;0]);
sharp_sensor_4_p2 = [0;sharp_sensor_range] + sharp_sensor_4_p1;
sharp_sensor_4=[sharp_sensor_4_p1;sharp_sensor_4_p2];

laser_matrix = basic_laser_model(origin_of_laser,rotation_matrix ) + [origin_of_vehicle;origin_of_vehicle];

% rearrange the matrix
sharp_sen_arr_pnts = [sharp_sensor_1, sharp_sensor_2, sharp_sensor_3, sharp_sensor_4];

sharp_sen_arr_pnts(1:2,:) = origin_of_vehicle + (rotation_matrix*(sharp_sen_arr_pnts(1:2,:)-origin_of_vehicle));
sharp_sen_arr_pnts(3:4,:) = origin_of_vehicle + (rotation_matrix*(sharp_sen_arr_pnts(3:4,:)-origin_of_vehicle));

corner_points = origin_of_vehicle + (rotation_matrix*(corner_points-origin_of_vehicle));
corner_points = corner_points;
% check intersection between laser lines and other objects
[rows, columns] = size(laser_matrix);
laser_intersection_matrix = [];
laser_distance_matrix = [];
for cntr = 1:columns
  
P1 = Line_Intersect_2d([laser_matrix(1:2,cntr),laser_matrix(3:4,cntr)],Ellipse_track_1);
P2 = Line_Intersect_2d([laser_matrix(1:2,cntr),laser_matrix(3:4,cntr)],Ellipse_track_2);

if ~isempty(P1)
  % laser line can intersect the object from multiple points.
  % Choose the closest point to vehicle, calculate the distance and save it to matrix
  dist_bw_points = ((laser_matrix(3:4,cntr)-P1)).^2;
  dist_bw_points = dist_bw_points(1,:)+dist_bw_points(2,:);
  dist_bw_points = dist_bw_points.^0.5;
  [element,index] = min(dist_bw_points); % get the index of minimum element
  laser_intersection_matrix =[ laser_intersection_matrix, P1(:,index)];
  laser_distance_matrix = [laser_distance_matrix, element];
  else
    laser_intersection_matrix =[ laser_intersection_matrix, [NaN;NaN]];
    laser_distance_matrix = [laser_distance_matrix, NaN];
  endif
  
  if ~isempty(P2)
  % laser line can intersect the object from multiple points.
  % Choose the closest point to vehicle, calculate the distance and save it to matrix
  dist_bw_points = ((laser_matrix(3:4,cntr)-P2)).^2;
  dist_bw_points = dist_bw_points(1,:)+ dist_bw_points(2,:);
  dist_bw_points = dist_bw_points.^0.5;
  [element,index] = min(dist_bw_points); % get the index of minimum element
    laser_intersection_matrix =[ laser_intersection_matrix, P2(:,index)];
    laser_distance_matrix = [laser_distance_matrix, element];
  else
    laser_intersection_matrix =[ laser_intersection_matrix, [NaN;NaN]];
    laser_distance_matrix = [laser_distance_matrix, NaN];
  endif
endfor

laser_measurement_matrix = inverse(rotation_matrix)*(laser_intersection_matrix- origin_of_vehicle) - origin_of_laser;



sigma_laser = abs(-0.000272.*laser_distance_matrix + 0.02);
sigma_laser = [sigma_laser;sigma_laser];
laser_error_matrix = normrnd(0,sigma_laser,[size(laser_measurement_matrix)]); 
laser_measurement_matrix = laser_measurement_matrix + laser_error_matrix;
laser_intersection_matrix = laser_intersection_matrix + laser_error_matrix;

% check intersection between sharp sensors and other objects
[rows,columns]=size(sharp_sen_arr_pnts);
sharp_intersection_matrix = [];
sharp_distance_matrix = [];
for cntr = 1:columns
  
P1 = Line_Intersect_2d([sharp_sen_arr_pnts(1:2,cntr),sharp_sen_arr_pnts(3:4,cntr)],Ellipse_track_1);
P2 = Line_Intersect_2d([sharp_sen_arr_pnts(1:2,cntr),sharp_sen_arr_pnts(3:4,cntr)],Ellipse_track_2);

if ~isempty(P1)
  
  sharp_intersection_matrix =[ sharp_intersection_matrix, P1(:,1)];
  %calculate measured distance for each sharp sensor
  dist = (sharp_sen_arr_pnts(1:2,cntr) - P1(:,1)).^2;
  dist = dist(1,:) + dist(2,:);
  sharp_distance_matrix = [ sharp_distance_matrix,  sqrt(dist)];
  endif
  
  if ~isempty(P2)
  
  sharp_intersection_matrix =[ sharp_intersection_matrix, P2(:,1)];
  %calculate measured distance for each sharp sensor
  dist = (sharp_sen_arr_pnts(1:2,cntr) - P2(:,1)).^2;
  dist = dist(1,:) + dist(2,:);
  sharp_distance_matrix = [ sharp_distance_matrix,  sqrt(dist)];

  endif
  
  if ((isempty(P1)) && (isempty(P2)))
    sharp_distance_matrix = [ sharp_distance_matrix,  500]; %% If line does not intersects any object, then put 500 mm measurement to array

  endif
  
  
endfor

% model errors of sharp sensor: http://www.gecad.isep.ipp.pt/iscies09/Papers/19November/iscies09_sharp_model.pdf

if (length(sharp_distance_matrix) ~= 0)
sigma_sharp = abs(-0.00272.*sharp_distance_matrix + 0.0304);
sharp_error_matrix = normrnd(0,sigma_sharp,[1,length(sharp_distance_matrix)]);
sharp_distance_matrix = sharp_distance_matrix + sharp_error_matrix;
quant_step = 500/512; % 500 mm, 9 bit adc , % add some quantization error
sharp_distance_matrix = floor(sharp_distance_matrix./quant_step);
endif



endfunction