figure

rotation_matrix = [cos(rotation_angle) -sin(rotation_angle); sin(rotation_angle) cos(rotation_angle)];

%inv_rot = inverse(rotation_matrix);
%
%las_points = inv_rot * laser_measurements_wo_NaN;
%
%scatter(las_points(1,:), las_points(2,:));

scatter(laser_measurements_wo_NaN(1,:), laser_measurements_wo_NaN(2,:));