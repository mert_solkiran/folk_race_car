clear
clc
close all
image_size = 100;
rgb_image = im2double(imread('gauss_test_image.bmp'));

gray_image = rgb_image(:,:,3);
% gray_image = imgaussfilt(gray_image,1);
F_1 = [-1 -1 -1; 0 0 0; 1 1 1];
conv_image = conv2(gray_image,F_1);
conv_image_2 = conv2(conv_image,F_1);

[y_size, x_size ] = size(gray_image);

sat_thres = 220/255;
F_2 = [-1, 0, 1];
index_mat = zeros(y_size,x_size);
% for (i=1:1:x_size)
% gray_image(:,i) = smoothts(gray_image(:,i), 'e', 10);
% conv_image_1d = conv(gray_image(:,i),F_2);
% conv_image_2_1d = conv(conv_image_1d,F_2);
% 
% index_1 = (gray_image(:,i)> sat_thres);
% index_2 = (conv_image_2_1d(3:end-2)< 0);
% index_of_row = index_1 & index_2 ;
% index_mat(:,i) = index_of_row;
%     if (~isempty(index_of_row))
%         
%     end
% end

% se = strel('square',5);
% index_mat_2 = imdilate(index_mat,se);
figure

subplot(2,2,1), imshow(gray_image);

subplot(2,2,2), surf(gray_image)

new_im = gray_image;

new_im(new_im<0.6) = 0;

subplot(2,2,3), imshow(new_im)
% 
subplot(2,2,4), surf(new_im)

col = gray_image(:,20);
figure
histogram(col,256) 

figure

new_im_2 = imgaussfilt(new_im,5);
subplot(1,2,1), imshow(new_im_2)
subplot(1,2,2), surf(new_im_2)
max(max(conv_image));

max(max(gray_image));