function [x_axis_top, x_axis_bottom, x_res, z_axis, z_res, standoff, angle_of_system]=laser_camera_module_v2(laser_offset_z, laser_offset_y, laser_rot_angle)

rot_point_laser_offset_z = laser_offset_z;
rot_point_laser_offset_x = 0;
rot_point_laser_offset_y = laser_offset_y;
rot_angle = laser_rot_angle;
rot_point_camera_offset = 0;
laser_depth = 200; % depth of laser line in mm, it is infinite physically but we are limiting it to create explicit problem
camera_view_depth = 200; % depth of view, mm, it is infinite physically but we are limiting it to create explicit problem

depth_flag = 1; % If depth of the laser and camera is not enough, algorithm fails. Depths will be increased until algortihm not fails or trial step constraint exceeded
trial_step = 100;

while ((depth_flag == 1) && (trial_step >0) && (laser_depth < 50000) && (camera_view_depth < 50000))

    close all


    [plane_of_cam_1, plane_of_cam_2, plane_of_cam_3, plane_of_cam_4, cam_org, lines_of_cam, HRES, VRES] = camera(camera_view_depth);
    [plane_of_laser] = laser(laser_depth);

    %% STANDART ROTATIONS

    rotate_z = 0;
    rotate_y = 0;
    rotate_x = -90;
    rot_org = [0,0,0];
    lines_of_cam = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, lines_of_cam);
    plane_of_cam_1 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_1);
    plane_of_cam_2 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_2);
    plane_of_cam_3 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_3);
    plane_of_cam_4 = Transformation3d([rotate_x;rotate_y;rotate_z], rot_org, plane_of_cam_4);

    %% STANDART ROTATIONS


    % laser offset
    offset_z = rot_point_laser_offset_z;
    offset_y = rot_point_laser_offset_y;
    plane_of_laser = Transformation3d([0;0;0], [0;offset_y;offset_z], plane_of_laser);

    % camera rotation
    rotate_x = rot_angle;
    rot_org = [0,0,0];
    lines_of_cam = Transformation3d([rotate_x;0;0], rot_org, lines_of_cam);
    plane_of_cam_1 = Transformation3d([rotate_x;0;0], rot_org, plane_of_cam_1);
    plane_of_cam_2 = Transformation3d([rotate_x;0;0], rot_org, plane_of_cam_2);
    plane_of_cam_3 = Transformation3d([rotate_x;0;0], rot_org, plane_of_cam_3);
    plane_of_cam_4 = Transformation3d([rotate_x;0;0], rot_org, plane_of_cam_4);

    % camera offset
    offset_z = rot_point_camera_offset;
    lines_of_cam = Transformation3d([0;0;0], [0;0;offset_z], lines_of_cam);
    plane_of_cam_1 = Transformation3d([0;0;0], [0;0;offset_z], plane_of_cam_1);
    plane_of_cam_2 = Transformation3d([0;0;0], [0;0;offset_z], plane_of_cam_2);
    plane_of_cam_3 = Transformation3d([0;0;0], [0;0;offset_z], plane_of_cam_3);
    plane_of_cam_4 = Transformation3d([0;0;0], [0;0;offset_z], plane_of_cam_4);

    [intersection_1,check_1] = plane_line_intersect(calculate_normal_of_plane(plane_of_laser),plane_of_laser(:,1),lines_of_cam(:,1),lines_of_cam(:,2));
    [intersection_2,check_2] = plane_line_intersect(calculate_normal_of_plane(plane_of_laser),plane_of_laser(:,1),lines_of_cam(:,1),lines_of_cam(:,3));
    [intersection_3,check_3] = plane_line_intersect(calculate_normal_of_plane(plane_of_laser),plane_of_laser(:,1),lines_of_cam(:,1),lines_of_cam(:,4));
    [intersection_4,check_4] = plane_line_intersect(calculate_normal_of_plane(plane_of_laser),plane_of_laser(:,1),lines_of_cam(:,1),lines_of_cam(:,5));
    if ((check_1 == 3) || (check_2 == 3) || (check_3 == 3) || (check_4 == 3)) %Intersection not found, depth will be increased
             
        laser_depth = laser_depth * 1.5; 
        camera_view_depth = camera_view_depth * 1.5;
        trial_step = trial_step -1; % decrease trial_step to prevent infinite loop because it is still possible that we are dealing with implicit problem
     endif
     if ((check_1 == 1) && (check_2 == 1) && (check_3 == 1) && (check_4 == 1) && (~isnan(intersection_1)) && (~isnan(intersection_2)) && (~isnan(intersection_3)) && (~isnan(intersection_4))) % Intersections found
        depth_flag = 0; % We found right depth values
        trial_step = 0; % we are going to stop iterations so no need another trial step
%        figure
%        hold on
%        plot3(plane_of_cam_1(1,:),plane_of_cam_1(2,:),plane_of_cam_1(3,:),'r',plane_of_cam_2(1,:),plane_of_cam_2(2,:),plane_of_cam_2(3,:),'r',plane_of_cam_3(1,:),plane_of_cam_3(2,:),plane_of_cam_3(3,:),'b',plane_of_cam_4(1,:),plane_of_cam_4(2,:),plane_of_cam_4(3,:),'b',plane_of_laser(1,:),plane_of_laser(2,:),plane_of_laser(3,:),'y')
%        % axis([-3000,3000,-3000,3000,-3000,3000])
%        xlabel('x') % x-axis label
%        ylabel('y') % x-axis label
%        zlabel('z') % x-axis label
%        grid on
%        axis equal 
        %alpha(0.5)
        %% end of cell plot

%        scatter3(intersection_1(1,:),intersection_1(2,:),intersection_1(3,:)); 
%        scatter3(intersection_2(1,:),intersection_2(2,:),intersection_2(3,:));
%        scatter3(intersection_3(1,:),intersection_3(2,:),intersection_3(3,:));
%        scatter3(intersection_4(1,:),intersection_4(2,:),intersection_4(3,:));
        x_axis_top = (sum((intersection_2 - intersection_1).^2)).^0.5;
        x_axis_bottom = (sum((intersection_3 - intersection_4).^2)).^0.5;
        delta_x = abs(x_axis_bottom - x_axis_top);
        mid_point_of_1_2 = (intersection_2 + intersection_1)./2;
        mid_point_of_3_4 = (intersection_3 + intersection_4)./2;
%        scatter3(mid_point_of_1_2(1,:),mid_point_of_1_2(2,:),mid_point_of_1_2(3,:));
%        scatter3(mid_point_of_3_4(1,:),mid_point_of_3_4(2,:),mid_point_of_3_4(3,:));
        z_axis = (sum((mid_point_of_1_2 - mid_point_of_3_4).^2)).^0.5;
        angle_of_system = atand((delta_x/2)/z_axis)*2;
        standoff = (sum((mid_point_of_3_4 - [0;0;rot_point_laser_offset_z]).^2)).^0.5; 
        dist_mid_top_to_org = (sum(mid_point_of_3_4.^2))^0.5;
        dist_mid_bottom_to_org = (sum(mid_point_of_1_2.^2))^0.5;
        x_res = x_axis_top / HRES;
        z_res = z_axis / VRES;

          
      endif
    
endwhile

if (depth_flag == 1)
%    disp("Suitable Depth value NOT found")  
    x_axis_top = 0;
    x_axis_bottom = 0;
    x_res = 0;
    z_axis = 0;
    z_res = 0;
    standoff = 0;
    angle_of_system = 0;
  endif
% scatter3(intersection_3(1,:),intersection_3(2,:),intersection_3(3,:)); 
% scatter3(intersection_4(1,:),intersection_4(2,:),intersection_4(3,:));


