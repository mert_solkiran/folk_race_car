function [ point ] = find_peak( column,width )


[row_val,I] = max(column);
column = 1-column; % inverse the values
min_val = I-width;
max_val = I+width;
if (min_val < 1)
    min_val = 1;
    if (max_val < 11)
        max_val = 11;
    end
end
if (max_val > length(column))
    max_val = length(column);
end
    
x = [min_val:max_val];
c1 = column(x);
f = fit(x',c1,'gauss3','MaxIter',100);
point = fminbnd(f,min(x),max(x)); % minimum of inverted values means maximum of function
% point = -1*point;
end

